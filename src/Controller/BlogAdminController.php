<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ArticleRepository;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Article;
use App\Form\ArticleType;


class BlogAdminController extends AbstractController{
/**
 * @Route("/home", name="home")
 */
 public function Blog_Admin()
 {
   return $this->render("home.html.twig",[]);
 }



/**
 * @Route("/createcompteadmin", name="create_compte_admin")
 */

public function Create_Compte_Admin()

{
  return $this->render("Create_Compte_Admin.html.twig",[]);
}

// -------------------
/**
 * @Route("/connexion", name="connexion")
 */

public function Connexion()
{
  return $this->render("Connexion.html.twig",[]);
}

// --------------------------
/**
 * @Route("/idoublies", name="identifiant_oublies")
 */

public function Idoublies()
{
  return $this->render("idoublies.html.twig",[]);
}
// -------------------
/**
 * @Route("/newid", name="new_identifiants")
 */
public function NewId()
{
  return $this->render("newid.html.twig",[]);
}
// ----------------------------
/**
 * @Route("/gestionprofil", name="gestion_profil")
 */

public function GestionProfil()
{
  return $this->render("gestionprofil.html.twig",[]);
}
// // -----------------------

// -----------------------
/**
 * @Route("/article", name="article")
 */

 
public function ArticleAdmin(ArticleRepository $repo)
{
  $result =$repo->GetAll();
  
  return $this->render("article.html.twig",[ 'result' =>$result]);

}
/**
 * @Route("/confirmersuppr/{id}", name="confirm_del")
 */
public function remove(int $id, ArticleRepository $repo) {
  $repo->delete($id);
  return $this->redirectToRoute("article");
}

// -------------------------

public function confirmersuppr()
{
  return $this->render("confirmersuppr.html.twig",[]);
}

}