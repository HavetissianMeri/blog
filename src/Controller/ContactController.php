<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Entity\Contact;

class ContactController extends AbstractController{

  /**
   * @Route("/contact",name="contact")
   */
public function new(Request $request){

$contact = new Contact();

$form = $this->createFormBuilder($contact)
        ->add('name',TextType::class) // add pour ajoute des champs et leurs types
        ->add('surname',TextType::class)
        ->add('message',TextType::class)
        ->add('save',SubmitType::class, array('label'=>'Contact'))//  ne pas oublie de lui dire de géner submit
        ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
    
          $contact = $form->getData();
          return $this->render('contact.html.twig', ["form" => $form->createView(),"contact" =>$contact] );
          
        }
        return $this->render('contact.html.twig', array('form' => $form->createView(),"contact"=>$contact
        ));

}


}

