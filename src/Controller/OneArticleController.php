<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\ArticleRepository;

class OneArticleController extends Controller
{
    /**
     * @Route("/one/article/{id}", name="one_article")
     */
    public function index(int $id, ArticleRepository $repo)
    {
            $id=$repo->getById($id);
        return $this->render('one_article/index.html.twig', [
            'row' => $id,
        ]);
    }
}
