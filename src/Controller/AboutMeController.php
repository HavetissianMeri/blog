<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


class AboutMeController extends AbstractController{
/**
 * @Route("aboutme", name="aboutme")
 */
 public function AboutMe()
 {
   return $this->render("aboutme.html.twig",[]);
 }

}