<?php
namespace App\Controller;

use App\Repository\ArticleRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Article;
use App\Form\ArticleType;

class ArticleController extends Controller
{
  /**
   * @Route("/add/article",name="add/article")
   */
public function Article(ArticleRepository $repo, Request $request)
{
  $art = new Article();

  $form=$this->createForm(ArticleType::class);

  // $repo->getAll($nouveauArticle);
  // dump($nouveauArticle);
  $form->handleRequest($request);

      
    if ($form->isSubmitted() && $form->isValid()) {

        $art = $form->getData();

        $art->date = new \DateTime();

        $repo->add($art);
        return $this->redirectToRoute("article");
        
    }
  return $this->render("creationArticle.html.twig", ['controller_name' => 'BlogAdminController', 'form'=>$form->createView() 
  ]);

}
  //  /**
  //    * @Route("/article/remove/{id}", name="removearticle")
  //    */
  //   public function remove(int $id, ArticleRepository $repo) {
  //     $repo->delete($id);
  //     return $this->redirectToRoute("home");
  // }


}