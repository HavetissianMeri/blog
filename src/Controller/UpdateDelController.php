<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\ArticleRepository;
use Symfony\Component\HttpFoundation\Request;
use App\Form\ArticleType;
use App\Entity\Article;

class UpdateDelController extends Controller
{
    /**
     *  @Route("/update/del/{id}", name="update_del")
     */
    
    public function index(int $id = null, ArticleRepository $repo, Request $request)
    {
        $article = $repo->getById($id);
        $form = $this->createForm(ArticleType::class, $article);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
                
            if ($repo->update($form->getData())) {
                return $this->redirectToRoute('article');
            }

        }
        return $this->render('update_del/index.html.twig', [
            'form' => $form->createView(),
            'article' => $article,
        ]);

    }

    /**
     * @Route("/article/remove/{id}", name="removearticle")
     */
    public function remove(int $id = null, ArticleRepository $repo)
    {
        $repo->delete($id);

        return $this->redirectToRoute("article");
    }
}