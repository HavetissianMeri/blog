<?php

namespace App\Repository;

use App\Entity\Article;
use App\Utils\ConnectUtil;



class ArticleRepository {
  public function GetAll():array 
  {

    $articleS=[];
    try { 
      $cnx = new \PDO("mysql:host=" . $_ENV["MYSQL_HOST"] . ":3306;dbname=" . $_ENV["MYSQL_DATABASE"], $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);
     
      $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION); 

      $query = $cnx->prepare("SELECT * FROM Article"); 
      $query->execute(); 

      foreach ($query->fetchAll() as $row) {
        $article = new Article();
        $article->fromSQL($row);
        $articleS[] = $article;// /**
        //  * @Route("/createarticle", name="createarticle")
        //  */
        
        // public function CreateArticle(ArticleRepository $repo, Request $request)
        // {
        //   $art = new Article();
        
        // $form=$this->createForm(ArticleType::class);
         
        //       // $nouveauArticle= new Article();
        //       // $nouveauArticle->title="name test php";
        //       // $nouveauArticle->contenu="race test php";
        //       // $nouveauArticle->date = \DateTime::createFromFormat("d/m/Y","24/09/1990");
        //       // $nouveauArticle->nb_vue="race test php";
        //       // $nouveauArticle->auteur="admin";
        
        //       // $repo->add($nouveauArticle);
        //       // dump($nouveauArticle);
        //       $form->handleRequest($request);
        
              
        //       if ($form->isSubmitted() && $form->isValid()) {
        
        
        //  $art=$repo->GetAll();
        //  $repo->add($dog);
        //  return $this->redirectToRoute("article");
        // }
        //   return $this->render("creationArticle.html.twig",[  'art' =>$art,'form' => $form->createView()]);
        
        // }
        
      }
    } catch (\PDOException $e) { 
      dump($e); 

    }
    return $articleS;
  }
  public function add(Article $article)
  {
    try {
      $cnx = new \PDO("mysql:host=" . $_ENV["MYSQL_HOST"] . ":3306;dbname=" . $_ENV["MYSQL_DATABASE"], $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);

      $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
      $query = $cnx->prepare("INSERT INTO Article (title, contenu, date, nb_vue, auteur) VALUES (:title, :contenu, :date, :nb_vue, :auteur)");
      

      $query->bindValue(":title", $article->title);
      $query->bindValue(":contenu", $article->contenu);
      $query->bindValue(":date", $article->date->format("Y-m-d"));
      $query->bindValue(":nb_vue", $article->nb_vue);
      $query->bindValue(":auteur", $article->auteur);


      $query->execute();
      $article->id = intval($cnx->lastInsertId()); 

    } catch (\PDOException $e) {
      dump($e);
    }
  }
  public function update(Article $article) {
    try {
        $cnx = ConnectUtil::getConnection();

        $query = $cnx->prepare("UPDATE Article SET title=:title, contenu=:contenu, date=:date, nb_vue=:nb_vue, auteur=:auteur  WHERE id=:id");
        
        $query->bindValue(":title", $article->title);
        $query->bindValue(":contenu", $article->contenu);
        $query->bindValue(":date", $article->date->format("Y-m-d"));
        $query->bindValue(":nb_vue", $article->nb_vue);
        $query->bindValue(":auteur", $article->auteur);
        $query->bindValue(":id", $article->id);


        return $query->execute();

    } catch (\PDOException $e) {
        dump($e);
    }
    return false;
}

public function delete(int $id) {
    try {
        $cnx = ConnectUtil::getConnection();
        $query = $cnx->prepare("DELETE FROM Article WHERE id=:id");
        
        $query->bindValue(":id", $id);

        return $query->execute();

    } catch (\PDOException $e) {
        dump($e);
    }
    return false;
}

public function getById(int $id): ?Article{
    try {
        $cnx = ConnectUtil::getConnection();

        $query = $cnx->prepare("SELECT * FROM Article WHERE id=:id");
        
        $query->bindValue(":id", $id);

        $query->execute();

        $result = $query->fetchAll();

        if(count($result) === 1) {
            $art = new Article();
            $art->fromSQL($result[0]);
            return $art;
        }

    } catch (\PDOException $e) {
        dump($e);
    }
    return null;
}
}
  