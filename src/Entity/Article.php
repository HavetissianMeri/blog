<?php

namespace App\Entity;



class Article
{
  public $id;
  public $title;
  public $contenu;
  public $date;
  public $nb_vue;
  public $auteur;

  public function fromSQL(array $sql)
  {
    $this->id = $sql["id"];
    $this->title = $sql["title"];
    $this->contenu = $sql["contenu"];
    $this->date = \DateTime::createFromFormat('Y-m-d', $sql["date"]);
    $this->nb_vue = $sql["nb_vue"];
    $this->auteur = $sql["auteur"];
    }
}